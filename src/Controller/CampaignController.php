<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 18.02.2018
 * Time: 20:43
 */

namespace App\Controller;

use App\Entity\Campaign;
use App\Entity\User;
use App\Factory\CampaignFactory;
use App\Factory\CampaignTypeFactory;
use App\Repository\CampaignRepository;
use App\Request\CreateCampaignRequest;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CampaignController
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var CampaignFactory
     */
    protected $campaignFactory;

    /**
     * @var CampaignTypeFactory
     */
    protected $campaignTypeFactory;

    /**
     * @var CampaignRepository
     */
    protected $campaignRepo;

    /**
     * CampaignController constructor.
     * @param ObjectManager $objectManager
     * @param CampaignFactory $campaignFactory
     * @param CampaignTypeFactory $campaignTypeFactory
     * @param CampaignRepository $campaignRepo
     */
    public function __construct(ObjectManager $objectManager, CampaignFactory $campaignFactory, CampaignTypeFactory $campaignTypeFactory, CampaignRepository $campaignRepo)
    {
        $this->objectManager = $objectManager;
        $this->campaignFactory = $campaignFactory;
        $this->campaignTypeFactory = $campaignTypeFactory;
        $this->campaignRepo = $campaignRepo;
    }

    public function cgetAction()
    {
        return $this->campaignRepo->findAll();
    }

    public function getAction($campaignId)
    {
        $campaign = $this->campaignRepo->find($campaignId);
        if (!$campaign instanceof Campaign) {
            throw new NotFoundHttpException('Campaign not found');
        }
        return $campaign;
    }

    public function postAction(CreateCampaignRequest $createCampaignRequest, User $user)
    {
        $campaignType = $this->campaignTypeFactory->create($createCampaignRequest->campaignTypeName);
        $this->objectManager->persist($campaignType);

        $campaign = $this->campaignFactory->create($createCampaignRequest->name, $user, $campaignType);
        $this->objectManager->persist($campaign);

        $this->objectManager->flush();

        return $campaign;
    }

    public function deleteAction($campaignId)
    {
        $campaign = $this->campaignRepo->find($campaignId);
        if (!$campaign instanceof Campaign) {
            throw new NotFoundHttpException('Campaign not found');
        }

        $this->objectManager->remove($campaign);
        $this->objectManager->flush();
    }

}
