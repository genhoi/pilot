<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 18.02.2018
 * Time: 20:08
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {

        return $this->render('base.html.twig');
    }

}
