<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 15.02.2018
 * Time: 7:23
 */

namespace App\ControllerListener;

use App\Entity\User;
use App\Repository\CampaignTokenRedisRepository;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CampaignTokenListener
{
    /**
     * @var CampaignTokenRedisRepository
     */
    protected $campaignTokenRepo;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * CampaignTokenListener constructor.
     * @param CampaignTokenRedisRepository $campaignTokenRepo
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CampaignTokenRedisRepository $campaignTokenRepo, TokenStorageInterface $tokenStorage)
    {
        $this->campaignTokenRepo = $campaignTokenRepo;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $requestSettings = explode('/', $request->getPathInfo());
        if ($requestSettings[1] !== 'api') {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!$token instanceof TokenInterface) {
            return;
        }

        $user = $token->getUser();
        if (!$user instanceof User) {
            return;
        }

        $campaignToken = $request->get('CampaignToken');
        $campaignId = $request->get('CampaignId');
        if ($campaignToken === null) {
            throw new BadRequestHttpException('Not found CampaignToken in HTTP Request');
        }
        if ($campaignId === null) {
            throw new BadRequestHttpException('Not found CampaignId in HTTP Request');
        }

        $repoCampaignToken = $this->campaignTokenRepo->get($user, $campaignId);

        if ($repoCampaignToken === false) {
            $repoCampaignToken = 0;
        }

        if ($repoCampaignToken >= $campaignToken) {
            throw new BadRequestHttpException('Campaign token already handled');
        }

        $this->campaignTokenRepo->set($user, $campaignId, $campaignToken);
    }

}
