<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 14.02.2018
 * Time: 0:07
 */

namespace App\ControllerListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationErrorsListener
{

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $validationErrors = $request->get('validationErrors');
        if ($validationErrors instanceof ConstraintViolationListInterface) {
            throw new BadRequestHttpException('Found validation errors');
        }
    }

}
