<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 11.02.2018
 * Time: 20:48
 */

namespace App\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Campaign
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="bigint")
     *
     * @var integer
     */
    protected $usersId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="users_id", referencedColumnName="id")
     *
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(type="bigint")
     *
     * @var integer
     */
    protected $campaignTypeId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CampaignType")
     * @ORM\JoinColumn(name="campaign_type_id", referencedColumnName="id")
     *
     * @var CampaignType
     */
    protected $campaignType;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="json_array", nullable=true, options={"jsonb": true})
     *
     * @var array
     */
    protected $customSetting;

    /**
     * @ORM\Column(type="datetimetz")
     *
     * @var \DateTime
     */
    protected $dateCreated;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     *
     * @var \DateTime
     */
    protected $dateUpdated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUsersId(): int
    {
        return $this->usersId;
    }

    /**
     * @param int $usersId
     */
    public function setUsersId(int $usersId): void
    {
        $this->usersId = $usersId;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getCampaignTypeId(): int
    {
        return $this->campaignTypeId;
    }

    /**
     * @param int $campaignTypeId
     */
    public function setCampaignTypeId(int $campaignTypeId): void
    {
        $this->campaignTypeId = $campaignTypeId;
    }

    /**
     * @return CampaignType
     */
    public function getCampaignType(): CampaignType
    {
        return $this->campaignType;
    }

    /**
     * @param CampaignType $campaignType
     */
    public function setCampaignType(CampaignType $campaignType): void
    {
        $this->campaignType = $campaignType;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getCustomSetting(): array
    {
        return $this->customSetting;
    }

    /**
     * @param array $customSetting
     */
    public function setCustomSetting(array $customSetting): void
    {
        $this->customSetting = $customSetting;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime $dateUpdated
     */
    public function setDateUpdated(\DateTime $dateUpdated): void
    {
        $this->dateUpdated = $dateUpdated;
    }

}
