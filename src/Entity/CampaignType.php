<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 11.02.2018
 * Time: 20:48
 */

namespace App\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Campaign
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CampaignTypeRepository")
 */
class CampaignType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    protected $status;

    /**
     * @ORM\Column(type="datetimetz")
     *
     * @var \DateTime
     */
    protected $dateCreated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

}
