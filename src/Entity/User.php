<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 11.02.2018
 * Time: 21:02
 */

namespace App\Entity;

use Doctrine\ORM\Mapping AS ORM;
use FOS\UserBundle\Model\User as FosUser;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class User
 * @package App\Entity
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User extends FosUser
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="datetimetz")
     *
     * @var \DateTime
     */
    protected $dateCreated;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     *
     * @var \DateTime
     */
    protected $dateUpdated;

    /**
     * @ORM\PrePersist
     */
    public function setDateCreatedForPrePersist()
    {
        $this->dateCreated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setDateUpdatedForPreUpdate()
    {
        $this->dateUpdated = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

}
