<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 13.02.2018
 * Time: 22:07
 */

namespace App\Factory;

use App\Entity\Campaign;
use App\Entity\CampaignType;
use App\Entity\User;

class CampaignFactory
{

    /**
     * @param $name
     * @param User $user
     * @param CampaignType $campaignType
     * @param array $customSetting
     *
     * @return Campaign
     */
    public function create($name, User $user, CampaignType $campaignType, $customSetting = null)
    {
        $campaign = new Campaign();
        $campaign->setCampaignType($campaignType);
        $campaign->setUser($user);
        $campaign->setName($name);
        $campaign->setDateCreated(new \DateTime());
        if ($customSetting) {
            $campaign->setCustomSetting($customSetting);
        }

        return $campaign;
    }

}
