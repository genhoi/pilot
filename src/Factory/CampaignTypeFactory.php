<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 13.02.2018
 * Time: 22:57
 */

namespace App\Factory;

use App\Entity\CampaignType;

class CampaignTypeFactory
{

    /**
     * @param $name
     * @return CampaignType
     */
    public function create($name)
    {
        $campaignType = new CampaignType();
        $campaignType->setName($name);
        $campaignType->setDateCreated(new \DateTime());
        $campaignType->setStatus(true);

        return $campaignType;
    }

}
