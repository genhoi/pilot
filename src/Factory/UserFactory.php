<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 13.02.2018
 * Time: 23:03
 */

namespace App\Factory;

use App\Entity\User;
use App\Request\CreateUserRequest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    /**
     * @var UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * UserFactory constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function createByRequest(CreateUserRequest $createRequest)
    {
        $user = new User();
        $user->setName($createRequest->name);
        $user->setEmail($createRequest->email);

        $encodedPassword = $this->encoder->encodePassword($user, $createRequest->password);
        $user->setPasswordHash($encodedPassword);

        return $user;
    }

}
