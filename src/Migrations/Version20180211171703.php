<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180211171703 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
          CREATE TABLE campaign (
            id BIGSERIAL NOT NULL PRIMARY KEY, 
            users_id BIGINT NOT NULL, 
            campaign_type_id BIGINT NOT NULL, 
            name VARCHAR(255) NOT NULL, 
            custom_setting JSONB DEFAULT NULL, 
            date_created TIMESTAMP(0) WITH TIME ZONE NOT NULL, 
            date_updated TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL
          )
        ');
        $this->addSql('CREATE INDEX IDX_campaign_users_id ON campaign (users_id)');
        $this->addSql('CREATE INDEX IDX_campaign_campaign_type_id ON campaign (campaign_type_id)');
        $this->addSql('COMMENT ON COLUMN campaign.custom_setting IS \'(DC2Type:json_array)\'');
        $this->addSql('
          CREATE TABLE campaign_type (
            id BIGSERIAL NOT NULL PRIMARY KEY, 
            name VARCHAR(255) NOT NULL, 
            status BOOLEAN NOT NULL, 
            date_created TIMESTAMP(0) WITH TIME ZONE NOT NULL
          )          
        ');

        $this->addSql('
          CREATE TABLE "users" (
            id BIGSERIAL NOT NULL PRIMARY KEY, 
            username VARCHAR(180) NOT NULL, 
            username_canonical VARCHAR(180) NOT NULL, 
            email VARCHAR(180) NOT NULL, 
            email_canonical VARCHAR(180) NOT NULL, 
            enabled BOOLEAN NOT NULL, 
            salt VARCHAR(255) DEFAULT NULL, 
            password VARCHAR(255) NOT NULL, 
            last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
            confirmation_token VARCHAR(180) DEFAULT NULL, 
            password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
            roles TEXT NOT NULL,
            date_created TIMESTAMP(0) WITH TIME ZONE NOT NULL, 
            date_updated TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL
          )
        ');
        $this->addSql('COMMENT ON COLUMN users.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E992FC23A8 ON users (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9A0D96FBF ON users (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9C05FB297 ON users (confirmation_token)');

        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_campaign_users_id_to_users_id FOREIGN KEY (users_id) REFERENCES "users" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_campaign_campaign_type_id_to_campaign_type_id FOREIGN KEY (campaign_type_id) REFERENCES campaign_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE campaign DROP CONSTRAINT FK_campaign_users_id_to_users_id');
        $this->addSql('ALTER TABLE campaign DROP CONSTRAINT FK_campaign_campaign_type_id_to_campaign_type_id');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE campaign_type');
        $this->addSql('DROP TABLE "users"');
    }
}
