<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 15.02.2018
 * Time: 7:19
 */

namespace App\Repository;

use App\Entity\User;

class CampaignTokenRedisRepository
{
    const NAMESPACE = 'tokens:campaign:';

    /**
     * @var \Redis
     */
    protected $redis;

    /**
     * CampaignTokenRedisRepository constructor.
     * @param \Redis $redis
     */
    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @param User $user
     * @param $campaignId
     * @param $token
     */
    public function set(User $user, $campaignId, $token)
    {
        $this->redis->set($this->getKey($user, $campaignId), $token);
    }

    /**
     * @param User $user
     * @param $campaignId
     * @return bool|string
     */
    public function get(User $user, $campaignId)
    {
        return $this->redis->get($this->getKey($user, $campaignId));
    }

    /**
     * @param User $user
     * @param $campaignId
     * @return string
     */
    protected function getKey(User $user, $campaignId)
    {
        return self::NAMESPACE . $user->getId() . ':' . $campaignId;
    }

}
