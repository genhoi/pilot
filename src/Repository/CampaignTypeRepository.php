<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 11.02.2018
 * Time: 21:11
 */

namespace App\Repository;

use App\Entity\CampaignType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CampaignTypeRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CampaignType::class);
    }

}
