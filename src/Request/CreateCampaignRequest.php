<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 14.02.2018
 * Time: 0:32
 */

namespace App\Request;

use Symfony\Component\Validator\Constraints as Assert;

class CreateCampaignRequest
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $campaignTypeName;

}
