<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 13.02.2018
 * Time: 23:08
 */

namespace App\Request;

use Symfony\Component\Validator\Constraints as Assert;

class CreateUserRequest
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan()
     *
     * @var string
     */
    public $password;

    /**
     * @Assert\Email()
     *
     * @var string
     */
    public $email;

}
