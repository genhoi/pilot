<?php
/**
 * Created by PhpStorm.
 * User: genho
 * Date: 14.02.2018
 * Time: 0:32
 */

namespace App\Request;

class UpdateCampaignRequest
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $campaignTypeName;

}
